import java.io.IOException;

public class ReadLineException extends Exception {
    public ReadLineException(String message) {
        super(message);
    }
}
